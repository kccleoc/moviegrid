package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

//APIDiscover get API response from Discover API of TMDB
func APIDiscover(options map[string]string) (*APIResponse, error) {
	var availableOptions = map[string]struct{}{
		"certification_country":    {},
		"certification":            {},
		"certification.lte":        {},
		"include_adult":            {},
		"include_video":            {},
		"language":                 {},
		"page":                     {},
		"primary_release_year":     {},
		"primary_release_date.gte": {},
		"primary_release_date.lte": {},
		"release_date.gte":         {},
		"release_date.lte":         {},
		"sort_by":                  {},
		"vote_count.gte":           {},
		"vote_count.lte":           {},
		"vote_average.gte":         {},
		"vote_average.lte":         {},
		"with_cast":                {},
		"with_crew":                {},
		"with_companies":           {},
		"with_genres":              {},
		"with_keywords":            {},
		"with_people":              {},
		"year":                     {}}
	optionsString := getOptionsString(options, availableOptions)
	uri := fmt.Sprintf("%s/discover/movie?api_key=%s%s", baseURL, apikey, optionsString)
	var results APIResponse
	result, err := getTmdb(uri, &results)
	return result.(*APIResponse), err
}

func getOptionsString(options map[string]string, availableOptions map[string]struct{}) string {
	var optionsString = ""
	for key, val := range options {
		if _, ok := availableOptions[key]; ok {
			newString := fmt.Sprintf("%s&%s=%s", optionsString, key, val)
			optionsString = newString
		}
	}
	return optionsString
}

func getTmdb(url string, payload interface{}) (interface{}, error) {
	var httpRequest http.Client
	const MaxReqPerSec = 4
	var (
		// hack: add some millisecond for don`t get 429 error
		rate     = time.Second/MaxReqPerSec + time.Millisecond*20
		throttle = time.Tick(rate)
	)
	var blocker <-chan time.Time

	httpRequest = getHTTPClient()
	blocker = throttle

	<-blocker

	res, err := httpRequest.Get(url)
	if err != nil { // HTTP connection error
		return payload, err
	}

	defer res.Body.Close() // Clean up

	body, err := ioutil.ReadAll(res.Body)
	if err != nil { // Failed to read body
		return payload, err
	}

	if res.StatusCode >= 200 && res.StatusCode < 300 { // Success!
		json.Unmarshal(body, &payload)
		return payload, nil
	}

	// Handle failure modes
	var status apiStatus
	err = json.Unmarshal(body, &status)
	if err != nil {
		return payload, err
	}
	return payload, fmt.Errorf("Code (%d): %s", status.Code, status.Message)
}

func getHTTPClient() http.Client {
	return http.Client{
		Transport: &http.Transport{},
	}
}
