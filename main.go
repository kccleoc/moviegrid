package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

const baseURL string = "https://api.themoviedb.org/3"

var apikey = os.Getenv("THEMOVIEDBAPIKEY")

type apiStatus struct {
	Code    int    `json:"status_code"`
	Message string `json:"status_message"`
}

// APIResponse is responses struct from API of TMDB
type APIResponse struct {
	Page         int `json:"page"`
	TotalResults int `json:"total_results"`
	TotalPages   int `json:"total_pages"`
	Results      []struct {
		VoteCount        int     `json:"vote_count"`
		ID               int     `json:"id"`
		Video            bool    `json:"video"`
		VoteAverage      float64 `json:"vote_average"`
		Title            string  `json:"title"`
		Popularity       float64 `json:"popularity"`
		PosterPath       string  `json:"poster_path"`
		OriginalLanguage string  `json:"original_language"`
		OriginalTitle    string  `json:"original_title"`
		GenreIds         []int   `json:"genre_ids"`
		BackdropPath     string  `json:"backdrop_path"`
		Adult            bool    `json:"adult"`
		Overview         string  `json:"overview"`
		ReleaseDate      string  `json:"release_date"`
	} `json:"results"`
}

func main() {

	var options = make(map[string]string)
	options["language"] = "en-US"

	res, err := APIDiscover(options)
	if err != nil {
		log.Fatalf("cannot get response from Discover [%s]", err)
	}

	jsonOutput(res)

}
func jsonOutput(res *APIResponse) {

	b, err := json.Marshal(res)
	if err != nil {
		fmt.Printf("could not nice print JSON [%s]", err)
	}

	fmt.Printf("%+s", b)
}
